# VUTTR (Very Useful Tools to Remember)

Este projeto utiliza Quarkus, o Supersonic Subatomic Java Framework.

Se quiser aprender mais sobre Quarkus, visite o site: https://quarkus.io/

## Requisitos - Desenvolvimento

- JDK 8 ou 11+ (qualquer distribuição): https://adoptopenjdk.net/
- Apache Maven 3.6.2+: https://maven.apache.org/

## Executando a aplicação em modo DEV

Para executar a aplicação em modo DEV com hot-reload utilize o seguinte comando:

```
./mvnw quarkus:dev
```

Para abrir a documentação do Swagger, é necessário estar em modo DEV e acessar http://localhost:3000/docs

## Compilando e executando a aplicação

A aplicação pode ser compilada utilizando: 

`./mvnw package`


O comando acima gera seguinte arquivo `vuttr-1.0.0-SNAPSHOT-runner.jar` que está no diretório `/target`.

A aplicação está pronta para executar com seguinte comando: 

`java -jar target/vuttr-1.0.0-SNAPSHOT-runner.jar`.

## Criando um executável nativo

Para criar um executável nativo execute seguinte comando:

`./mvnw package -Pnative`

Caso não tenha o GraalVM instalado, pode executar o build executável nativo em um container usando:

`./mvnw package -Pnative -Dquarkus.native.container-build=true`.

You can then execute your native executable with: `./target/vuttr-1.0.0-SNAPSHOT-runner`

Quer saber mais sobre o build de executáveis nativos, consulte: https://quarkus.io/guides/building-native-image
