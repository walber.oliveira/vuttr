package br.com.walloliveira.vuttr.resources.tools;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ToolsResourceTest {

	@Test
	public void testToolsResourceNewInstance() {
		var exception = assertThrows(IllegalCallerException.class, ToolsResource::new);
		assertThat(exception.getMessage()).isEqualTo("This class not must be instantiate");
	}
}
