package br.com.walloliveira.vuttr.resources;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.*;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.exceptions.DistributionException;
import de.flapdoodle.embed.process.runtime.Network;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import java.io.IOException;
import java.net.UnknownHostException;


public abstract class AbstractIntegrationTest {

	private static final String BIND_IP = "localhost";
	private static MongodExecutable mongodExecutable;
	private static final int PORT = 12345;

	@BeforeAll
	static void setUp() throws Exception {
		var starter = MongodStarter.getDefaultInstance();
		var cmdOptions = newMongoCmdOptions();
		var mongodConfig = buildMongodConfig(cmdOptions);
		try {
			if (mongodExecutable != null && mongodExecutable.isRegisteredJobKiller()) {
				return;
			}
			mongodExecutable = starter.prepare(mongodConfig);
			mongodExecutable.start();
		} catch (DistributionException | IOException e) {
			e.printStackTrace();
		}
	}

	private static IMongodConfig buildMongodConfig(
		IMongoCmdOptions cmdOptions
	) throws IOException {
		return new MongodConfigBuilder()
			       .version(Version.Main.PRODUCTION)
			       .net(newNet())
			       .cmdOptions(cmdOptions)
			       .build();
	}

	@NotNull
	private static Net newNet() throws UnknownHostException {
		return new Net(
			BIND_IP,
			PORT,
			Network.localhostIsIPv6()
		);
	}

	private static IMongoCmdOptions newMongoCmdOptions() {
		return new MongoCmdOptionsBuilder()
			       .useNoJournal(false)
			       .useSmallFiles(true)
			       .build();
	}

	@AfterAll
	static void tearDown() {
		mongodExecutable.stop();
	}
}
