package br.com.walloliveira.vuttr.resources.tools.domain;

import br.com.walloliveira.vuttr.resources.AbstractIntegrationTest;
import br.com.walloliveira.vuttr.utils.valueObjects.url.UrlVO;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
@Transactional
public class ToolsRepositoryTest extends AbstractIntegrationTest {

	private final ToolsRepository toolsRepository = new ToolsRepository();

	@Test
	@DisplayName(
		"Given that the tool is persisted " +
			"and the tags values are node and java " +
			"and the tag value for search is javascript " +
			"when method findByTagVO is called " +
			"then the return must be empty"
	)
	public void testFindToolsByTagAndReturnIsEmpty() {
		var tools = Tools.of(
			UrlVO.of("http://localhost"),
			"Title",
			"description",
			Set.of(TagVO.of("node"), TagVO.of("java"))
		);
		toolsRepository.persist(tools);

		List<Tools> list = toolsRepository.findByTagVO(TagVO.of("javascript"));
		assertThat(list).hasSize(0);
	}

	@Test
	@DisplayName(
		"Given that the two tool are persisted " +
			"and the tags values are node and java " +
			"and the tag value for search is node " +
			"when method findByTagVO is called " +
			"then the return must be one"
	)
	public void testFindToolsByTag() {
		var tools1 = Tools.of(
			UrlVO.of("http://localhost"),
			"Title",
			"description",
			Set.of(TagVO.of("node"), TagVO.of("java"))
		);
		var tools2 = Tools.of(
			UrlVO.of("http://localhost"),
			"Title",
			"description",
			Set.of(TagVO.of("node"), TagVO.of("java"))
		);
		toolsRepository.persist(tools1);
		toolsRepository.persist(tools2);

		List<Tools> list = toolsRepository.findByTagVO(TagVO.of("node"));
		assertThat(list).hasSize(2);
	}

	@BeforeEach
	public void tearDown() {
		toolsRepository.deleteAll();
	}
}
