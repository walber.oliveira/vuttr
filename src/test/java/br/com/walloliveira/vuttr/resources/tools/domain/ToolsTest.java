package br.com.walloliveira.vuttr.resources.tools.domain;

import br.com.walloliveira.vuttr.utils.valueObjects.url.UrlVO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Set;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class ToolsTest {

	@ParameterizedTest
	@MethodSource("urlVOAndTags")
	@DisplayName(
		"Given that the parameters are valid" +
			"when the 'of' method of Tools class is called" +
			"then the object of Tools class must be created"
	)
	void testCreateToolsInstance(UrlVO urlVO, Set<TagVO> tags) {
		var tools = Tools.of(
			urlVO,
			"Title",
			"Description",
			tags
		);

		assertThat(tools.title).isEqualTo("Title");
		assertThat(tools.description).isEqualTo("Description");
		assertThat(tools.link).isEqualTo("http://localhost:8888/test");
		assertThat(tools.tags).hasSize(2);
		assertThat(tools.tags).isEqualTo(Set.of("node", "java"));
	}

	static Stream<Arguments> urlVOAndTags() {
		return Stream.of(
			arguments(
				UrlVO.of("http://localhost:8888/test"),
				Set.of(
					TagVO.of("node"),
					TagVO.of("java")
				)
			),
			arguments(
				UrlVO.of("http://localhost:8888/test"),
				Set.of(
					TagVO.of("node"),
					TagVO.of("java")
				)
			)
		);
	}
}
