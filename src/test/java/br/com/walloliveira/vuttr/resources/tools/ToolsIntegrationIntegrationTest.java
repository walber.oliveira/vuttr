package br.com.walloliveira.vuttr.resources.tools;

import br.com.walloliveira.vuttr.resources.AbstractIntegrationTest;
import br.com.walloliveira.vuttr.resources.tools.domain.Tools;
import br.com.walloliveira.vuttr.resources.tools.domain.ToolsRepository;
import br.com.walloliveira.vuttr.resources.tools.requests.CreateToolsRequest;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
@Transactional
public class ToolsIntegrationIntegrationTest extends AbstractIntegrationTest {
	private static final String DELETE_PATH = ToolsResource.PATH + "/%s";

	@Inject
	private ToolsRepository toolsRepository;

	@Test
	public void testPostToolsResource() {
		var payload = new CreateToolsRequest();
		payload.tags = Set.of("node", "java");
		payload.description = "Description";
		payload.title = "Title";
		payload.link = "http://localhost";
		var body = given()
			           .that()
			           .auth()
			           .basic("jhin", "4fires")
			           .body(payload)
			           .contentType(ContentType.JSON)
			           .post(ToolsResource.PATH)
			           .then()
			           .statusCode(201)
			           .and()
			           .extract()
			           .body()
			           .as(Tools.class);
		assertThat(body.tags).hasSize(2);
		assertThat(body.tags).isEqualTo(Set.of("node", "java"));
		assertThat(body.link).isEqualTo("http://localhost");
		assertThat(body.title).isEqualTo("Title");
		assertThat(body.description).isEqualTo("Description");
		assertThat(body.id).isNotNull();
	}

	@Test
	public void testDeleteToolsResource() {
		var payload = new CreateToolsRequest();
		payload.tags = Set.of("node", "java");
		payload.description = "Description";
		payload.title = "Title";
		payload.link = "http://localhost";
		var body = given()
			           .that()
			           .auth()
			           .basic("jhin", "4fires")
			           .body(payload)
			           .contentType(ContentType.JSON)
			           .post(ToolsResource.PATH)
			           .then()
			           .statusCode(201)
			           .and()
			           .extract()
			           .body()
			           .as(Tools.class);
		var path = buildPath(body.id);
		given()
			.that()
			.auth()
			.basic("jhin", "4fires")
			.when()
			.delete(path)
			.then()
			.statusCode(204);
	}

	@Test
	public void testDeleteToolsResourceWithInvalidId() {
		String path = buildPath("123");
		var response = given()
			               .that()
			               .auth()
			               .basic("jhin", "4fires")
			               .when()
			               .delete(path)
			               .then()
			               .statusCode(400)
			               .extract()
			               .as(Map.class);
		assertThat(response.get("message")).isEqualTo("ID is invalid");
	}

	@Test
	public void testDeleteToolsResourceAndToolNotFound() {
		var objectId = new ObjectId();
		var path = buildPath(objectId);
		var response = given()
			               .that()
			               .auth()
			               .basic("jhin", "4fires")
			               .when()
			               .delete(path)
			               .then()
			               .statusCode(404)
			               .extract()
			               .as(Map.class);
		assertThat(response.get("message")).isEqualTo("Tools not found");
	}

	@Test
	public void testGetAllTools() {
		var tool1 = new CreateToolsRequest();
		tool1.tags = Set.of("node", "java");
		tool1.description = "Description";
		tool1.title = "Title";
		tool1.link = "http://localhost";
		var tool2 = new CreateToolsRequest();
		tool2.tags = Set.of("node", "java");
		tool2.description = "Description";
		tool2.title = "Title";
		tool2.link = "http://localhost";

		given()
			.that()
			.auth()
			.basic("jhin", "4fires")
			.body(tool1)
			.contentType(ContentType.JSON)
			.post(ToolsResource.PATH)
			.then()
			.statusCode(201);
		given()
			.that()
			.auth()
			.basic("jhin", "4fires")
			.body(tool2)
			.contentType(ContentType.JSON)
			.post(ToolsResource.PATH)
			.then()
			.statusCode(201);
		var response = given()
			               .that()
			               .auth()
			               .basic("jhin", "4fires")
			               .when()
			               .get(ToolsResource.PATH)
			               .then()
			               .statusCode(200)
			               .extract()
			               .as(List.class);
		assertThat(response).hasSize(2);
	}

	@Test
	public void testGetToolsByTagNode() {
		var tool1 = new CreateToolsRequest();
		tool1.tags = Set.of("java");
		tool1.description = "Description";
		tool1.title = "Title";
		tool1.link = "http://localhost";
		var tool2 = new CreateToolsRequest();
		tool2.tags = Set.of("node", "java");
		tool2.description = "Description";
		tool2.title = "Title";
		tool2.link = "http://localhost";

		given()
			.that()
			.auth()
			.basic("jhin", "4fires")
			.body(tool1)
			.contentType(ContentType.JSON)
			.post(ToolsResource.PATH)
			.then()
			.statusCode(201);
		given()
			.that()
			.auth()
			.basic("jhin", "4fires")
			.body(tool2)
			.contentType(ContentType.JSON)
			.post(ToolsResource.PATH)
			.then()
			.statusCode(201);
		var response = given()
						   .that()
			               .auth()
			               .basic("jhin", "4fires")
			               .param("tag", "node")
			               .when()
			               .get(ToolsResource.PATH)
			               .then()
			               .statusCode(200)
			               .extract()
			               .as(List.class);
		assertThat(response).hasSize(1);
	}

	private String buildPath(Object o) {
		return String.format(DELETE_PATH, o);
	}

	@AfterEach
	public void tearDown() {
		toolsRepository.deleteAll();
	}
}
