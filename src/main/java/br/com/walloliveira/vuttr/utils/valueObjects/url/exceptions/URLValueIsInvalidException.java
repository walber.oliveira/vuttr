package br.com.walloliveira.vuttr.utils.valueObjects.url.exceptions;

public final class URLValueIsInvalidException extends RuntimeException {

    /**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public URLValueIsInvalidException(String url) {
        super(String.format("The link value is invalid: %s", url));
    }
}
