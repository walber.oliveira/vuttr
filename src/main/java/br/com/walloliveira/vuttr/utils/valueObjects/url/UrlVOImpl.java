package br.com.walloliveira.vuttr.utils.valueObjects.url;

import br.com.walloliveira.vuttr.utils.valueObjects.url.exceptions.URLValueIsInvalidException;
import org.jetbrains.annotations.NotNull;

import java.net.MalformedURLException;
import java.net.URL;

final class UrlVOImpl implements UrlVO {
	private final String link;

	UrlVOImpl(@NotNull final String link) {
		try {
			var url = new URL(link);
			this.link = url.toString();
		} catch (MalformedURLException e) {
			throw new URLValueIsInvalidException(link);
		}
	}

	public String getLink() {
		return link;
	}
}
