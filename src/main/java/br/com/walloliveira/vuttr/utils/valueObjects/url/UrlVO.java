package br.com.walloliveira.vuttr.utils.valueObjects.url;

import org.jetbrains.annotations.NotNull;

public interface UrlVO {

	String getLink();

	static UrlVO of(@NotNull final String value ) {
		return new UrlVOImpl(value);
	}
}
