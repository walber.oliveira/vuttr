package br.com.walloliveira.vuttr.exceptionMappers;

import br.com.walloliveira.vuttr.exceptionMappers.responses.ResponseError;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.security.InvalidParameterException;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

@Provider
public final class InvalidParameterExceptionMapper implements ExceptionMapper<InvalidParameterException> {
	@Override
	public Response toResponse(InvalidParameterException e) {
		return Response
			       .status(BAD_REQUEST)
			       .entity(ResponseError.of(e.getMessage()))
			       .build();
	}
}
