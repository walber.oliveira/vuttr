package br.com.walloliveira.vuttr.resources.tools;

import br.com.walloliveira.vuttr.exceptionMappers.responses.ResponseError;
import br.com.walloliveira.vuttr.resources.tools.domain.ToolsRepository;
import org.bson.types.ObjectId;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.security.InvalidParameterException;

import static javax.ws.rs.core.Response.Status.NOT_FOUND;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = ToolsResource.NAME)
@Path(ToolsResource.PATH)
@RolesAllowed("Admin")
public class DeleteToolsResource {
    private ToolsRepository toolsRepository;

    public DeleteToolsResource() {
    }

    @Inject
    public DeleteToolsResource(final ToolsRepository toolsRepository) {
        this();
        this.toolsRepository = toolsRepository;
    }

    @Transactional
    @DELETE
    @Path(ToolsResource.ID_PATH)
    public Response delete(@PathParam(ToolsResource.ID_PARAM) String id) {
        if (!toolWasDeleted(id)) {
            return Response
                    .status(NOT_FOUND)
                    .entity(ResponseError.of("Tools not found"))
                    .build();
        }
        return Response.noContent()
                .build();
    }

    private boolean toolWasDeleted(String id) {
        if (!ObjectId.isValid(id)) {
            throw new InvalidParameterException("ID is invalid");
        }
        var objectId = new ObjectId(id);
        return this.toolsRepository.deleteById(objectId);
    }
}
