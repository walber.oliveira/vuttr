package br.com.walloliveira.vuttr.resources.tools.domain;


import io.quarkus.mongodb.panache.PanacheMongoRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class ToolsRepository implements PanacheMongoRepository<Tools> {

	public List<Tools> findByTagVO(TagVO tag) {
		return find("tags", tag.getValue())
			       .list();
	}
}