package br.com.walloliveira.vuttr.resources.tools;

import br.com.walloliveira.vuttr.resources.tools.domain.TagVO;
import br.com.walloliveira.vuttr.resources.tools.domain.Tools;
import br.com.walloliveira.vuttr.resources.tools.domain.ToolsRepository;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static com.google.common.base.Strings.isNullOrEmpty;
import static javax.ws.rs.core.Response.Status.OK;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = ToolsResource.NAME)
@Path(ToolsResource.PATH)
@RolesAllowed("Admin")
public class GetToolsResource {

    private ToolsRepository toolsRepository;

    public GetToolsResource() {
    }

    @Inject
    public GetToolsResource(final ToolsRepository toolsRepository) {
        this();
        this.toolsRepository = toolsRepository;
    }

    @GET
    public Response get(@QueryParam(ToolsResource.TAG_PARAM) final String tag) {
        var toolsList = getToolsByTag(tag);
        return Response.status(OK)
                .entity(toolsList)
                .build();
    }

    private List<Tools> getToolsByTag(final String tag) {
        if (isNullOrEmpty(tag)) {
            return toolsRepository
                    .findAll()
                    .list();
        }
        return toolsRepository.findByTagVO(TagVO.of(tag));
    }
}