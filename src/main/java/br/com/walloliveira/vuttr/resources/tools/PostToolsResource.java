package br.com.walloliveira.vuttr.resources.tools;

import br.com.walloliveira.vuttr.resources.tools.domain.ToolsRepository;
import br.com.walloliveira.vuttr.resources.tools.requests.CreateToolsRequest;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.CREATED;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = ToolsResource.NAME)
@Path(ToolsResource.PATH)
@RolesAllowed("Admin")
public class PostToolsResource {

    private ToolsRepository toolsRepository;

    public PostToolsResource() {
    }

    @Inject
    public PostToolsResource(final ToolsRepository toolsRepository) {
        this();
        this.toolsRepository = toolsRepository;
    }

    @Transactional
    @POST
    public Response post(@Valid final CreateToolsRequest request) {
        var tools = request.toDomain();
        this.toolsRepository.persist(tools);
        return Response.status(CREATED)
                .entity(tools)
                .build();
    }
}
