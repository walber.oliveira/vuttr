package br.com.walloliveira.vuttr.resources.tools.domain;

import br.com.walloliveira.vuttr.resources.tools.domain.exceptions.TagValueIsNullOrEmptyException;
import org.jetbrains.annotations.NotNull;

import static com.google.common.base.Strings.isNullOrEmpty;

public class TagVO {
	private final String value;

	private TagVO(@NotNull final String tag) {
		if (isNullOrEmpty(tag)) {
			throw new TagValueIsNullOrEmptyException();
		}
		this.value = tag;
	}

	public static TagVO of(String value) {
		return new TagVO(value);
	}

	public String getValue() {
		return value;
	}
}
