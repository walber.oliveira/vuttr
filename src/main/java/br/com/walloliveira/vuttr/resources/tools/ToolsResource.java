package br.com.walloliveira.vuttr.resources.tools;

public final class ToolsResource {
	public static final String PATH = "/tools";
	public static final String NAME = "Tools";
	public static final String ID_PATH = "/{id}";
	public static final String ID_PARAM = "id";
	public static final String TAG_PARAM = "tag";

	ToolsResource() {
		throw new IllegalCallerException("This class not must be instantiate");
	}
}
