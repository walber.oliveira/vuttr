package br.com.walloliveira.vuttr.resources.tools.domain;

import br.com.walloliveira.vuttr.utils.valueObjects.url.UrlVO;
import com.google.common.collect.Sets;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@MongoEntity(collection = "TheTools")
public class Tools extends PanacheMongoEntity {

	public String link;

	public String title;

	public String description;

	public Set<String> tags = Sets.newHashSet();

	public Tools() {
	}

	private Tools(
		String link,
		String title,
		String description,
		Set<String> tags
	) {
		this();
		this.link = link;
		this.title = title;
		this.description = Optional.ofNullable(description)
			                   .orElse("");
		this.tags.addAll(tags);
	}

	public static Tools of(
		final UrlVO urlVO,
		final String title,
		final String description,
		final Set<TagVO> tags
	) {
		return new Tools(
			urlVO.getLink(),
			title,
			description,
			tags
				.stream()
				.map(TagVO::getValue)
				.collect(Collectors.toSet())
		);
	}
}
