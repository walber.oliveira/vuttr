package br.com.walloliveira.vuttr;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeIn;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.eclipse.microprofile.openapi.annotations.security.SecuritySchemes;

import javax.ws.rs.core.Application;

@OpenAPIDefinition(
        info = @Info(
                title = "Very Useful Tools to Remember",
                version = "1.1.1",
                contact = @Contact(
                        name = "WalberOliveira",
                        url = "https://app.bossabox.com/u/walber-oliveira",
                        email = "walberjr.oliveira@gmail.com")
        ),
        security = {@SecurityRequirement(name = "Authorization")}
)
@SecuritySchemes(
        @SecurityScheme(
                in = SecuritySchemeIn.HEADER,
                securitySchemeName = "Authorization",
                type = SecuritySchemeType.HTTP,
                scheme = "basic"
        )
)
public class ApiApplication extends Application {

}
